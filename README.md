# README #

This Repository is for Archiving and Uploading Schematic Files for the Robot that is being used for the 2017 IEEE South-East Conference in North Carolina.  The SCH Files were made in AutoDesk EAGLE.

### What is this repository for? ###

* These are AutoDesk EAGLE Schematic Files.  You need AutoDesk EAGLE to view the Schematic.  Otherwise, the file will open up as a Human-Readable text file that you may or may not understand.  If you don't want that to happen, then get AutoDesk EAGLE.
* Version 2017.1 (Probably the Only Version needed)

### How do I get set up? ###

* __Summary of set up:__
    1. Download the SCH File.
    2. Add the SCH File to your project.
    3. Open the SCH File.

* __Dependencies:__
    - Requires AutoDesk EAGLE - [ Order Now ](http://www.autodesk.com/education/free-software/eagle)

### Who do I talk to? ###

* Chris Woodle (Team Lead)
* Max Rodriguez (Repo Creator and guy who wrote this README)